<?php
class SuperNews
{
    public function __construct($filename) {
        $this->sourceName = $filename;
	}
	
    /* type string */
    private $sourceName;
    /* type array */
	private $allNews;
	
	private function getNewsFileContent(){
	    $jsonData = file_get_contents ($this->sourceName);
	    $this->allNews = json_decode( $jsonData, true);
	}
	
     /* return string */
    public function getAllNews($entryHeader){
        $this->getNewsFileContent();
      if(empty($this->allNews)) return '';
       $newsList = '<section style="width: 80%; margin: 50px auto;"><h1>'.$entryHeader.'</h1>';
        foreach($this->allNews as $oneNew => $oneNewDetail){
           $newsList .='<hr style="margin: 30px auto;"><article>';
           $newsList .='<h2>'.$oneNew.'</h2>';
           $newsList .='<div>'.nl2br($oneNewDetail['content']).'</div>';
           $newsList .='<div>'.$oneNewDetail['author'].'</div>';
           $newsList .='<div>'.$oneNewDetail['date'].'</div>';
           $newsList .='</article>';
        }
        $newsList .= '</section>';
        return $newsList;
	}
}
$news = new SuperNews(__DIR__.'/data/news.json');
echo $news->getAllNews('Our company news');
