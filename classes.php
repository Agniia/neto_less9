<?php
class SuperAuto
{
    public function __construct($price, $vinCode) {
	       $this->setItsPrice($price);
	       $this->setItsVINCode($vinCode);
	}
    private $itsHexColor = '#fff';
    private $itsPrice;	
    private $itsCurrencyCode = 'EUR';
    private $itsVINCode; 

    public function getItsHexColor() {
        return $this->itsHexColor;
    }
    public function setItsHexColor($color) {
        $this->itsHexColor = $color;
    }
    
    public function getItsPrice() {
        return $this->itsPrice;
    }
    public function setItsPrice($price) {
        $this->itsPrice = $price;
    }

    public function getItsCurrencyCode() {
        return $this->itsCurrencyCode;
    }
    public function setItsCurrencyCode($currCode) {
        $this->itsCurrencyCode = $currCode;
    }
    
    public function getItsVINCode() {
        return $this->itsVINCode;
    }
    public function setItsVINCode($vinCode) {
        $this->itsVINCode = $vinCode;
    }
}
$auto1 = new SuperAuto(6000,'1GNEK13ZX3R298984');
$auto1->setItsCurrencyCode('USD');
echo '<pre>';
var_dump($auto1);
$auto2 = new SuperAuto(10000,'5XXGN4A70CG022862');
$auto2->setItsHexColor('#000');
echo '<pre>';
var_dump($auto2);

class TelevisionReceiver
{
    public function __construct($kind, $vendor) {
	    $this->setItsKind($kind); 
	    $this->setItsVendor($vendor); 
	}
    private $itsVendor;
    private $itsKind;
    private $itsScreenDiagonal = 45;
    
     /*
        1) CRT — телевизоры с электронно-лучевой трубкой;
        2) RP — телевизоры с задней проекцией;
        3) DLP — проекционные телевизоры;
        4) LCD — жидкокристаллические телевизоры;
        5) PDP — плазменные панели;
        6) LED — телевизоры на светодиодах.
    */
    private static $kindsOfTV = array('CRT', 'RP', 'DLP', 'LCD', 'PDP', 'LED');
    
    public static function getKindsOfTV() {
        return self::$kindsOfTV;
    }
    public static function setKindsOfTV($kind) {
        self::$kindsOfTV[] = $kind;
    }   
    
    public function getItsKind() {
        return $this->itsKind;
    }
    public function setItsKind($kind) {
        $this->itsKind = $kind;
    }    
        
    public function getItsVendor() {
        return $this->itsVendor;
    }
    public function setItsVendor($vendor) {
        $this->itsVendor = $vendor;
    }
            
    public function getItsScreenDiagonal() {
        return $this->itsScreenDiagonal;
    }
    public function setItsScreenDiagonal($screenDiagonal) {
        $this->itsScreenDiagonal = $screenDiagonal;
    }
}
$tv1 = new TelevisionReceiver(TelevisionReceiver::getKindsOfTV()[1],'Samsung');
$tv1->setItsScreenDiagonal(65);
var_dump($tv1);
$tv2 = new TelevisionReceiver(TelevisionReceiver::getKindsOfTV()[3],'Sony');
$tv2->setItsScreenDiagonal(45);
var_dump($tv2);

class BallPen
{
    public function __construct($color, $vendor, $price) {
	    $this->setItsHexColor($color); 
	    $this->setItsVendor($vendor); 
	    $this->setItsPrice($price);
	}
	
    private $itsHexColor;
    private $itsVendor;
    private $itsPrice;	
    
    public function getItsHexColor() {
        return $this->itsHexColor;
    }
    public function setItsHexColor($color) {
        $this->itsHexColor = $color;
    }
        
    public function getItsVendor() {
        return $this->itsVendor;
    }
    public function setItsVendor($vendor) {
        $this->itsVendor = $vendor;
    }   
                
    public function getItsPrice() {
        return $this->itsPrice;
    }
    public function setItsPrice($price) {
        $this->itsPrice = $price;
    }
}
$pen1 = new BallPen('#0000FF','Pilot',60);
var_dump($pen1);
$pen2 = new BallPen('#000','Parker',40);
var_dump($pen2);

class ZooDuck
{
    public function __construct($breed,$healthStatus) {
        $this->setItsBreed($breed); 
        $this->setItsHealthStatus($healthStatus);
	}
	
    private $itsBreed;
    private $itsConditionsOfDetention;
    private $itsHealthStatus;
    
    public function getItsBreed() {
        return $this->itsBreed;
    }
    public function setItsBreed($breed) {
        $this->itsBreed = $breed;
    }
        
    public function getItsConditionsOfDetention() {
        return $this->itsConditionsOfDetention;
    }
    public function setItsConditionsOfDetention($conditions) {
        $this->itsConditionsOfDetention = $conditions;
    }
           
    public function getItsHealthStatus() {
        return $this->itsHealthStatus;
    }
    public function setItsHealthStatus($healthStatus) {
        $this->itsHealthStatus = $healthStatus;
    }
}
$duck1 = new ZooDuck('Anas platyrhynchos','чихает');
$duck1->setItsConditionsOfDetention('место, похожее на \'Тиффани\', желательно, подальше от тополей');
var_dump($duck1);
$duck2 = new ZooDuck('Tadorna','переела');
$duck2->setItsConditionsOfDetention('тепло и солнечно, желательно, Аргентина и клубника со сливками');
var_dump($duck2);

class SuperProduct
{
    public function __construct($price,$vendor,$vendorCode) {
        $this->setItsPrice($price);
        $this->setItsVendor($vendor);
        $this->setItsVendorCode($vendorCode);
	}
    
    private $itsPrice;	
    private $itsCurrencyCode = 'RUB'; 
    private $itsVendor;	
    private $itsVendorCode;	        
	        
    public function getItsPrice() {
        return $this->itsPrice;
    }
    public function setItsPrice($price) {
        $this->itsPrice = $price;
    }
    
    public function getItsCurrencyCode() {
        return $this->itsCurrencyCode;
    }
    public function setItsCurrencyCode($code) {
        $this->itsCurrencyCode = $code;
    }
            
    public function getItsVendor() {
        return $this->itsVendor;
    }
    public function setItsVendor($vendor) {
        $this->itsVendor = $vendor;
    }
                
    public function getItsVendorCode() {
        return $this->itsVendorCode;
    }
    public function setItsVendorCode($vendorCode) {
        $this->itsVendorCode = $vendorCode;
    }
}    
$prod1 = new SuperProduct(1000,'Pilot','qwfiwnf19023');
$prod1->setItsCurrencyCode('EUR');
var_dump($prod1);
$prod2 = new SuperProduct(4000,'Pilot','JIEFnj19023');
var_dump($prod2);

